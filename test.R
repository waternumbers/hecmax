### Boilerplate R code for Lancaster HEC via slurm
### Use the boilerplate job script that passes the 
### job info on the Rscript call

### Get the command line arguments.
args <- commandArgs(TRUE)
job_name <- args[1]
task <- args[2]
job_id <- args[3]

# Send messages
message("This is job ", job_name, " with task ",task," and id ",job_id)

# Construct the path to the data output directory
output_dir <- file.path("Outputs",job_name,job_id)

# Construct the data output file name
output_name <- paste0(job_name,"_",task,".rds")

# The full path for saving our data..
output_file <- file.path(output_dir, output_name)

# Show where
message("Output is at ", output_file)

# Do our work here...
data = runif(10)

# Save our data output
saveRDS(data,file=output_file)


